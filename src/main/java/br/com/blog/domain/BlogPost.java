package br.com.blog.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Data
@Entity
public class BlogPost {

    @Id
    @GeneratedValue
    private Long id;

    private String title;

    private String content;

    @Column(name = "created_at")
    private LocalDateTime creationTimestamp;

    @Column(name = "updated_at")
    private LocalDateTime lastUpdateTimeStamp;
}
