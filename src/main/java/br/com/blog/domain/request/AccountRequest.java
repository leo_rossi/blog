package br.com.blog.domain.request;

import lombok.Data;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@Data
public class AccountRequest {

    @Email(message = "Please insert a valid email")
    @NotEmpty(message = "The field 'email' can't be empty")
    private String email;

    @NotEmpty(message = "The field 'username' can't be empty")
    @Pattern(regexp = "^[a-zA-Z0-9_.]*$", message = "Username must contains only alphanumeric characters, dots and underscores")
    private String username;

    private String fullName;

    @NotEmpty(message = "The field 'password' can't be empty")
    private String password;

    @NotEmpty(message = "The field 'matchingPassword can't be empty")
    private String matchingPassword;

    @AssertTrue(message = "The passwords aren't matching")
    private boolean machingPasswords() {
        return password.equals(matchingPassword);
    }
}
