package br.com.blog.controller;

import br.com.blog.domain.request.AccountRequest;
import br.com.blog.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping(name = "/users/register")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @GetMapping
    public String index() {
        return "register";
    }

    @PostMapping
    public ModelAndView register(@ModelAttribute("account") @Valid AccountRequest accountRequest,
                                 BindingResult bindingResult,
                                 RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return new ModelAndView("/register").addObject(accountRequest);
        }
        accountService.save(accountRequest);
        return new ModelAndView("redirect:/login");
    }
}
