package br.com.blog.repository;

import br.com.blog.domain.BlogPost;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BlogpostRepository extends JpaRepository<BlogPost, Long> {

    Optional<BlogPost> findByTitle(String title);
}
