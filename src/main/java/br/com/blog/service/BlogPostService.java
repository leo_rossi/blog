package br.com.blog.service;

import br.com.blog.domain.BlogPost;

import java.util.List;

public interface BlogPostService {

    void save(BlogPost post);

    List<BlogPost> findAll();

    BlogPost findById(Long id);

    BlogPost findByTitle(String title);
}
