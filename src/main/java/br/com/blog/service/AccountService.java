package br.com.blog.service;

import br.com.blog.domain.request.AccountRequest;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface AccountService extends UserDetailsService {

    void save(AccountRequest accountVO);

}
