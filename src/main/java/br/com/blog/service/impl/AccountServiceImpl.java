package br.com.blog.service.impl;

import br.com.blog.domain.Account;
import br.com.blog.repository.AccountRepository;
import br.com.blog.service.AccountService;
import br.com.blog.domain.request.AccountRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.info("Method=loadUserByUsername, Param={}", username);

        Account account = accountRepository.findByEmail(username);
        if (account == null) {
            throw new UsernameNotFoundException("User not found");
        }

        return account;
    }

    public void save(AccountRequest accountRequest) {

    }
}
